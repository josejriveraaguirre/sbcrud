package com.delta.sbcrud.repos;

import com.delta.sbcrud.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {

    @Query("from Employee e where e.id like ?1")
    Employee getEmployeeById(long id);
}
