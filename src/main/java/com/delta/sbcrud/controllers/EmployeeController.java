package com.delta.sbcrud.controllers;

import com.delta.sbcrud.models.Employee;
import com.delta.sbcrud.repos.EmployeeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("/employees")
    public String showEmployees(Model model) {
        List<Employee> employeeList = employeeRepository.findAll();
        model.addAttribute("noEmployeesFound", employeeList.size() == 0);
        model.addAttribute("employees", employeeList);
        return "employees/index";

    }

    @GetMapping("/employees/create")
    public String showForm(Model model) {
        model.addAttribute("newEmployee", new Employee());
        return "employees/create";
    }

    @PostMapping("/employees/create")
    public String createEmployee(@ModelAttribute Employee employeeToCreate) {
        employeeRepository.save(employeeToCreate);
        return "redirect:/employees/";
    }

//    @GetMapping("/employees/{id}")
//    public String showEmployee(@PathVariable long id, Model model) {
//        Employee employee = employeeRepository.getOne(id);
//        model.addAttribute("showEmployee", employee);
//        return "/employees/show";
//    }

    @GetMapping("employees/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        Employee employeeToEdit = employeeRepository.getOne(id);
        model.addAttribute("editEmployee", employeeToEdit);
        return "employees/edit";
    }

    @PostMapping("/employees/{id}/edit")
    public String updateEmployee(@PathVariable long id,
                             @RequestParam(name ="firstName") String firstName,
                             @RequestParam(name = "lastName") String lastName,
                             @RequestParam(name = "employeeNumber") Long employeeNumber,
                             @RequestParam(name = "username") String username,
                             @RequestParam(name = "email") String email) {

        Employee foundEmployee = employeeRepository.getEmployeeById(id);

        foundEmployee.setFirstName(firstName);
        foundEmployee.setLastName(lastName);
        foundEmployee.setEmployeeNumber(employeeNumber);
        foundEmployee.setUsername(username);
        foundEmployee.setEmail(email);

        employeeRepository.save(foundEmployee);

        return "redirect:/employees/";
    }

    @PostMapping("/employees/{id}/delete")
    public String delete(@PathVariable long id) {

        employeeRepository.deleteById(id);

        return "redirect:/employees/";
    }

} // end of class



